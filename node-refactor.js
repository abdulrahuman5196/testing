// Removed Unused variables and imports ,Created an object instead of having multiple variables.
// First value of arrays of songs is label. 0th postion is for label.
var songsList = {
    imagine :['easy', 'c', 'cmaj7', 'f', 'am', 'dm', 'g', 'e7'],
    somewhere_over_the_rainbow : ['easy','c', 'em', 'f', 'g', 'am'],
    tooManyCooks : ['easy','c', 'g', 'f'],
    iWillFollowYouIntoTheDark : ['medium','f', 'dm', 'bb', 'c', 'a', 'bbm'],
    babyOneMoreTime : ['medium','cm', 'g', 'bb', 'eb', 'fm', 'ab'],
    creep : ['medium', 'g', 'gsus4', 'b', 'bsus4', 'c', 'cmsus4', 'cm6'],
    paperBag : ['hard','bm7', 'e', 'c', 'g', 'b7', 'f', 'em', 'a', 'cmaj7',
        'em7', 'a7', 'f7', 'b'],
    toxic : ['hard','cm', 'eb', 'g', 'cdim', 'eb7', 'd7', 'db7', 'ab', 'gmaj7',
        'g7'],
    bulletproof: ['hard','d#m', 'g#', 'b', 'f#', 'g#m', 'c#']
};
//Variables
var songs = [],
    labels = [],
    allChords = [],
    labelCounts = [],
    labelProbabilities = [],
    chordCountsInLabels = {},
    probabilityOfChordsInLabels = {};

// Also I have renamed i to ii and j to jj in loops. Reason:  Searching ii is faster/better than searching i where there is huge code in a file
// This is my personal convention to improve code quality.
function train(chords, label) {
    songs.push([label, chords]);
    labels.push(label);
    for (var ii = 1; ii < chords.length; ii++){// starting i at 1;
        if(!allChords.includes(chords[ii])) {
            allChords.push(chords[ii]);
        }
    }
    //Condensed if else statement using ternary operator. removed !!
    Object.keys(labelCounts).includes(label) ? labelCounts[label] +=1 : labelCounts[label]=1;
}

function getNumberOfSongs() {
    return songs.length;
}

function setLabelProbabilities() {
    Object.keys(labelCounts).forEach(function(label) {
        var numberOfSongs = getNumberOfSongs();
        labelProbabilities[label] = labelCounts[label] / numberOfSongs;
    });
}

function setChordCountsInLabels() {
    songs.forEach(function(ii){
        if(chordCountsInLabels[ii[0]] === undefined) {
            chordCountsInLabels[ii[0]] = {};
        }
        ii[1].forEach(function(jj) {
            //Condensed if else statement using ternary operator.
            (chordCountsInLabels[ii[0]][jj] > 0)? chordCountsInLabels[ii[0]][jj] += + 1 : chordCountsInLabels[ii[0]][jj] = 1;
        });
    });
}

function setProbabilityOfChordsInLabels() {
    probabilityOfChordsInLabels = chordCountsInLabels;
    Object.keys(probabilityOfChordsInLabels).forEach(function(ii) {
        Object.keys(probabilityOfChordsInLabels[ii]).forEach(function(jj) {
            probabilityOfChordsInLabels[ii][jj] *= 1.0 / songs.length;
        });
    });
}

// condensed calling train function with Looping through the Songlist object . value at 0th position is label for the song.
for (var song in songsList) {
    train(songsList[song], songsList[song][0]);
}

setLabelProbabilities();
setChordCountsInLabels();
setProbabilityOfChordsInLabels();

function classify(chords) {
    var ttal = labelProbabilities;
    //I am not sure if this should be kept or not I usually like to have console.log statements eliminated from code unless they are printing something useful
    var classified = {};
    Object.keys(ttal).forEach(function(obj) {
        var first = ttal[obj] + 1.01;
        chords.forEach(function(chord) {
            var probabilityOfChordInLabel =
                probabilityOfChordsInLabels[obj][chord];
            // consensed if else.
            (probabilityOfChordInLabel === undefined) ? first +1.01 : first *= (probabilityOfChordInLabel + 1.01);
        });
        classified[obj] = first;
    });
    console.log(classified);
}

classify(['d', 'g', 'e', 'dm']);
classify(['f#m7', 'a', 'dadd9', 'dmaj7', 'bm', 'bm7', 'd', 'f#m']);